'use strict';

describe('Controller: ApiV1Ctrl', function () {

  // load the controller's module
  beforeEach(module('mawioApp'));

  var ApiV1Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ApiV1Ctrl = $controller('ApiV1Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ApiV1Ctrl.awesomeThings.length).toBe(3);
  });
});
