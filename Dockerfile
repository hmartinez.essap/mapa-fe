FROM nginx
MAINTAINER Arturo Volpe <arturovolpe@gmail.com>

ADD ./utils/docker/default.conf /etc/nginx/conf.d/default.conf
ADD ./utils/docker/nginx.conf /etc/nginx/nginx.conf

ADD dist /dist


WORKDIR /usr/share/nginx/

RUN rm -rf html && mv /dist /usr/share/nginx/html
