'use strict';

angular.module('mapaFeApp').factory('LoginService', function (url, $http, $q) {

  /**
  * Obtiene un elemento del localStorage, si es json lo parsea
  */
  function get(key) {
    var toRet = localStorage.getItem(key);
    if (toRet === null) return null;
    if (toRet.indexOf('{') === 0) return JSON.parse(toRet);
    return toRet;
  }

  /**
  * Guarda un elemento en el store, si es json lo convierte primero a string
  */
  function set(key, value) {
    if (value === null) return;
    return localStorage.setItem(key, angular.isObject(value) ? JSON.stringify(value) : value);
  }


  function saveSettings(data) {
    if (!data) {
      data = {
        token : get('token'),
        user : get('user')
      };
    }
    set('user', data.user);
    set('token', data.token);
    $.ajaxSetup({
      headers    : {
        Authorization : 'Bearer ' + data.token
      }
    });
  }

  function hasRol(rol){
    var tieneRol = false;
    angular.forEach(get('user').roles, function(rolUsuario) {
      if (rolUsuario.rol === rol) tieneRol = true;
    })
    return tieneRol;
  }

  function resetSettings() {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
  }

  return {
    login : function(email, pass) {
      return $http({
        method  : 'POST',
        url     : url + '/authentication',
        data    : $.param({
          username : email,
          password : pass
        }),
        headers : {
          'Content-Type' : 'application/x-www-form-urlencoded'
        }
      }).then(function success(response) {
        saveSettings(response.data);
        return response;
      }, function failure(response) {
        resetSettings();
        return $q.reject(response);
      });
    },

    logout : function() {
      resetSettings();
      return $q.resolve();
    },

    checkIfLogged : function() {
      if (!localStorage.getItem('token'))
        return $q.reject();
      saveSettings();

      return $http({
        method : 'GET',
        url : url + '/authentication/isLogged'
      }).then(function success(response) {
        return response;
      }, function failure(response) {
        resetSettings();
        return $q.reject(response);
      });

    },
    hasRol : hasRol,
    get    : get,
    getUser : function() {
      return get("user");
    }
  };
});
