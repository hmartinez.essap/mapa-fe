'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('EstadosShapesService', function (url, $http) {
    return {
      getAll : function() {
        return $http.get(url + '/estados/all');
      }
    };
  });
