'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('UsuariosService', function (url, $http, $location) {
    return {

      getById : function(id) {
        return $http.get(url + '/usuario/' + id);
      },

      addOrUpdate: function(data) {
        if (data.id)
          return $http.put(url + '/usuario/' + data.id, data);
        else
          return $http.post(url + '/usuario', data);
      },

      delete : function(id) {
        return $http.delete(url + '/usuario/' + id);
      },

      ajaxConfig : function() {
         return {
           url  : url + '/usuario/',
           type : 'GET',
           error : function(error) {
             if (error.status === 401) {
               $location.path("/mapa");
             }
           }
         };
       }

    };
  });
