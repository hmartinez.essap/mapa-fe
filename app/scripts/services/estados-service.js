  'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').factory('EstadosService', function (url, $http) {
    return {

      getById : function(id) {
        return $http.get(url + '/estados/' + id);
      },

      getAll: function(){
        return $http.get(url + '/estados/all');
      },

      addOrUpdate : function(data) {
        if (data.id)
          return $http.put(url + '/estados/' + data.id, data);
        else
          return $http.post(url + '/estados', data);
      },

      delete : function(id) {
        return $http.delete(url + '/estados/' + id);
      },

      ajaxConfig : function() {
         return {
           url  : url + '/estados/',
           type : 'GET'
         };
       }

    };
  });
