'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('ProveedoresService', function (url, $http) {
    return {
      getAll : function() {
        return $http.get(url + '/proveedores/all');
      },

      getProveedor : function(idProveedor){
        return $http.get(url + '/proveedores/' + idProveedor);
      },

      addOrUpdate: function(idProveedor, proveedor){
        if (!proveedor.id) {
          return $http.post(url + '/proveedores/', proveedor);
        } else {
          return $http.put(url + '/proveedores/' + idProveedor, proveedor);
        }
      },

      delete: function(idProveedor){
        return $http.delete(url + '/proveedores/' + idProveedor);
      },

      ajaxConfig : function() {
        return {
          url  : url + '/proveedores/',
          type : 'GET'
        };
      }
    }
  });
