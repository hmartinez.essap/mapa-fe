'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('ComentariosService', function (url, $http) {
    return {
      getAll : function() {
        return $http.get(url + '/comentarios');
      },
      getComentario : function(codigo){
        return $http.get(url + '/comentarios/byCodigo/' + codigo);
      },
      getComentarioById : function(id){
        return $http.get(url + '/comentarios/' + id);
      },
      add : function(comentario){
        return $http.post(url + '/comentarios/public', {
            shapeId : comentario.shapeId,
            mensaje : comentario.comentario,
            email   : comentario.email,
            telefono: comentario.telefonoContacto,
            latitud : comentario.posicion.lat,
            longitud: comentario.posicion.lng,
            captcha : comentario.captcha
          });
      },
      update: function(idComentario, comentario){
        return $http.put(url + '/comentarios/' + idComentario, comentario);
      },
      delete: function(idComentario){
        return $http.delete(url + '/comentarios/' + idComentario);
      },

      responder: function(codigoComentario, respuesta) {
        return $http.post(url + '/comentarios/byCodigo/' + codigoComentario + '/responder', {
          message : respuesta
        });
      },

      ajaxConfig : function() {
        return {
          url  : url + '/comentarios/',
          type : 'GET'
        };
      }
    }
  });
