'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('DepartamentosService', function (url, $http) {
    return {
      getDistritos : function(dpto) {
        return $http.get(url + '/departamento/' + dpto + '/distritos/');
      },

      getBarrios: function(dpto,distrito){
        return  $http.get(url + '/departamento/' + dpto + '/distrito/'+ distrito + '/barrios');
      }
    }
  });
