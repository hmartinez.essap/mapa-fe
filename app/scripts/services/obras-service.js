'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('ObrasService', function (url, $http) {
    return {
      getAll : function() {
        return $http.get(url + '/obras/');
      },
      getObra : function(idObra){
        return $http.get(url + '/obras/' + idObra);
      },

      addOrUpdate: function(idObra, obra){
        if (!obra.id) {
          return $http.post(url + '/obras/', obra);
        } else {
          return $http.put(url + '/obras/' + idObra, obra);
        }
      },

      delete: function(idObra){
        return $http.delete(url + '/obras/' + idObra);
      },

      getShapeDeContratos : function(idObra, filtros){
        return $http.get(url + '/obras/' + idObra + '/shapes?' + $.param({
          dpto : filtros.departamento,
          distrito : filtros.distrito,
          localidad: filtros.barrio
        }));
      },

      getAllShapes : function(idObra) {
        return $http.get(url + '/obras/' + idObra + '/allshapes');
      },

      ajaxConfig : function() {
        return {
          url  : url + '/obras/',
          type : 'GET'
        };
      }
    };
  });
