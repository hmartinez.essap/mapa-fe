'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').factory('TiposService', function (url, $http) {
    return {

      getById : function(id) {
        return $http.get(url + '/tipos/' + id);
      },

      getAll: function(){
        return $http.get(url + '/tipos/all');
      },

      addOrUpdate : function(data) {
        if (data.id)
          return $http.put(url + '/tipos/' + data.id, data);
        else
          return $http.post(url + '/tipos', data);
      },

      delete : function(id) {
        return $http.delete(url + '/tipos/' + id);
      },

      ajaxConfig : function() {
         return {
           url  : url + '/tipos/',
           type : 'GET'
         };
       }

    };
  });
