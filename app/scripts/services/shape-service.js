'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('ShapesService', function (url, $http) {


    return {

      getShape : function(idObra) {
        return $http.get(url + '/obras/'+ idObra+ '/shapes');
      },
      getAll:function(){
        return $http.get(url + '/shape/all/');
      },

      getAllFiltered:function(filtros, tipo, offset, limit, order){

        if (!offset) offset = 0;
        if (!limit) limit = -1;

        var parametrosJson = {
          dpto                : filtros.departamento ? filtros.departamento.codigo : null,
          distrito            : filtros.distrito     ? filtros.distrito.codigo     : null,
          localidad           : filtros.barrio       ? filtros.barrio.barrioCod    : null,
          offset              : offset,
          limit               : limit,
          order               : order
        };

        if (filtros.proveedor) {
          parametrosJson.proveedorId = filtros.proveedor;
        }
        var parametros = $.param(parametrosJson);

        if(tipo == 'list') {
          return $http.get(url + '/shape/all/?' + parametros);
        }

        if(tipo == 'list-json') {
          return $http.get(url + '/shape/all/json?' + parametros);
        }

        if(tipo == 'CSV') {
          parametrosJson.order=undefined;
          window.location.href = url + '/shape/all/csv?' + parametros;
        }
        if(tipo == 'JSON') {
          parametrosJson.order=undefined;
          window.location.href = url + '/shape/all/json?' + parametros;
        }
      },

      getInfo:function(idShape){
          return $http.get(url + '/shape/'+ idShape +'/info');
      },

      getShape:function(idShape){
          return $http.get(url + '/shape/'+ idShape +'/shape');
      },

      updateShapesEnObras:function(idObra, shape){
        return $http.post(url + '/obras/' + idObra + '/shapes', shape);
      },
      updateShapesEnContratos:function(idObra,idContrato,shape){
        return $http.post(url + '/obra/' + idObra + '/contrato/'+idContrato+'/shapes', shape);
      },
      getAllDepartamentos : function() {
        return $http.get(url + '/departamento/all');
      }
    }
  });
