'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('FotosService', function (url, $http) {
    return {
      getAll : function() {
        return $http.get(url + '/obras/all');
      },

      getFotos : function(idShape) {
        return $http.get(url + '/shape/' + idShape + '/images');
      },

      addImage:function(idShape,imagen){
        var fd = new FormData();
        fd.append('image', imagen);
        $http.post(url + '/shape/' + idShape + '/image', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
      },

      deleteImage : function(idImagen){
        $http.delete(url + '/foto/' + idImagen);
      }
    };
  });
