'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .factory('ContratosService', function (url, $http) {
    return {
      getAll : function(idObra) {
        return $http.get(url + '/obra/' + idObra + '/contrato/');
      },
      getContrato : function(idObra, idContrato){
        return $http.get(url + '/obra/' + idObra + '/contrato/'+ idContrato);
      },
      addOrUpdate: function(idObra, contrato){
        if (!contrato.id) {
          return $http.post(url + '/obra/' + idObra + '/contrato', contrato);
        } else {
          return $http.put(url + '/obra/' + idObra + '/contrato/' + contrato.id, contrato);
        }
      },
      getContratoDNCP : function(nroContratacion, codContratacion){
        var parametros = {};
        if(nroContratacion && codContratacion){
          parametros.nroDNCP = nroContratacion;
          parametros.codDNCP = codContratacion;
        }
        if(nroContratacion){
          parametros.nroDNCP = nroContratacion;
        }
        if(codContratacion){
          parametros.codDNCP = codContratacion;
        }
        return $http.get(url + '/obras/contratosDNCP?'+
          $.param(parametros));
      },
      delete: function(idContrato){
        return $http.delete(url + '/obra/-1/contrato/' + idContrato);
      },
      getAllShapes: function(idObra, idContrato){
          return $http.get(url + '/obra/' + idObra + '/contrato/'+ idContrato+'/allShapes');
      },
      ajaxConfig : function(obra) {
        return {
          url  : url + '/obra/' + obra + '/contrato',
          type : 'GET'
        };
      }
    }
  });
