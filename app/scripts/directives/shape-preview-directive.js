'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('shapePreview', function () {
  return {
    templateUrl : 'scripts/directives/shape-preview-directive.html',
    restrict : 'E',
    scope: true,
    link : function(scope, element, attrs) {
      $scope.center = {
        zoom : 13,
        lat  : -25.2986595,
        lng  : -57.5914783
      };
    }
  }
});
