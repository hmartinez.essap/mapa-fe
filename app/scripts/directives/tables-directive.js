'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('tables', function($compile, dtI18n, growl, $timeout) {
  return {
    templateUrl: 'scripts/directives/tables-directive.html',
    restrict: 'E',
    scope: {
      columns: '=',
      options: '=',
      service: '='
    },
    link: function(scope, element, attrs) {

      function renderButtons(element, data, row) {

        function getLinkButton(text, linkTo) {
          return '<a href="' + linkTo + '" class="btn btn-info">' + text + '</a>';
        }

        function getActionButton(text, action) {
          return '<a ng-click="' + action + '" class="btn btn-danger">' + text + '</a>';
        }

        if (row.id === -1000) return;
        var buttons = [];

        buttons.push('<div class="btn-group">');

        if (scope.options.withEditar === true || angular.isUndefined(scope.options.withEditar)) {
          var editPath = scope.options.editPath.replace('ID', row.id);
          buttons.push(getLinkButton('Editar', editPath));
        }
        if (scope.options.withEliminar === true || angular.isUndefined(scope.options.withEliminar)) {
          buttons.push(getActionButton('Eliminar', 'borrar(' + row.id + ',\'' + row[scope.options.borrarProperty] + '\')'));
        }


        if (angular.isFunction(scope.options.otherButtons)) {
          scope.options.otherButtons(buttons, row, getLinkButton, getActionButton);
        }

        buttons.push('</div>');
        $(element).append($compile(buttons.join(''))(scope));
      }

      scope.columns.push({
        title: 'Acciones',
        data: 's',
        defaultContent: '',
        sortable: false,
        filtrable: false
      });

      scope.puedeAgregar = angular.isUndefined(scope.options.withAgregar) ? true : scope.options.withAgregar;

      if (!angular.isFunction(scope.options.getAjaxConfig)) {
        scope.options.getAjaxConfig = scope.service.ajaxConfig;
      }

      $(document).ready(function() {
        $('.datepicker').datepicker({
          format: "dd/mm/yyyy",
          autoclose: true,
        })
      });

      var oTable = element.find('.table').DataTable({
        dom: 'Blrtip',
        order: [
          [0, 'desc']
        ],
        processing: true,
        serverSide: true,
        responsive: true,
        columns: scope.columns,
        ajax: scope.options.getAjaxConfig(),
        language: dtI18n,
        columnDefs: [{
          targets: -1,
          createdCell: renderButtons,
          responsivePriority: 1
        }]
      });

      var filter = _.chain(scope.columns)
        .forEach(function(value, index) {
          value.idx = index;
        })
        .forEach(function(value) {
          value.defaultContent = '';
        })
        .filter(function(value) {
          return value.filtrable || value.filtrable === undefined;
        })
        .map(function(value) {
          if (angular.isDefined(value.filter_config)) {
            return value.filter_config({
              column_number: value.idx,
              filter_reset_button_text: false,
              filter_default_label: 'Seleccionar...',
              select_type: 'select',
              style_class: 'form-control select2',
              select_type_options: {
                minimumResultsForSearch: -1
              }
            });
          } else {
            return {
              column_number: value.idx,
              filter_type: value.filter_type || 'text',
              filter_default_label: value.filter_type ? ['Desde', 'Hasta'] : value.title,
              style_class: ['form-control'],
              filter_reset_button_text: false,
              filter_delay: 500,
              date_format: 'dd/mm/yyyy'
            };
          }
        }).value();


      yadcf.init(oTable, filter);


      scope.borrar = function(id, nombre) {
        $('#deleteModal').modal('show');
        scope.aBorrar = {
          id: id,
          nombre: nombre
        };
      };

      scope.doBorrar = function() {
        scope.service.delete(scope.aBorrar.id).then(function() {
          growl.success('Registro eliminado con éxito');
          $('#deleteModal').modal('hide');
          oTable.ajax.reload();
        },function error(mensaje) {
          growl.error('El registro seleccionado no puede ser eliminado');
        });
      };
    }
  };
});
