'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('mawioHasRol', function (LoginService) {
  return {
    restrict : 'A',
    link : function(scope, element, attr) {
      var tieneRol = LoginService.hasRol(attr.mawioHasRol);
      if (!tieneRol)
        element.addClass('ng-hide');
    }
  };
});
