'use strict';

angular.module('mapaFeApp').directive('abmImagenesDirective', function ($rootScope, LoginService, growl) {
  return {
    templateUrl : 'scripts/directives/abm-imagenes-directive.html',
    restrict : 'E',
    scope: {
      uploader:"=",
      images:"=",
      obraContratoActual:"=obraContratoActual"
    },
    controller : function($scope, $http,FileUploader, FotosService,$routeParams, $route, $timeout, ShapesService, ObrasService) {

      $scope.reloadRoute=function(){
          $route.reload();
      };


      $scope.btnEliminarShapesSeleccionados = function(){
          $('#eliminarFotosModal').modal('show');
      }

      $scope.cantidadAEliminar=0;

      $scope.contadorEliminar = function(checkboxValue){
        if(checkboxValue){
            $scope.cantidadAEliminar ++;
        }else{
          $scope.cantidadAEliminar --;
        }
      }

      $scope.eliminarFotos = function(){
        $('#eliminarFotosModal').modal('hide');
        angular.forEach($scope.images,function(imagen){
          if (imagen.eliminar) {
            FotosService.deleteImage(imagen.id);
          }
        });
        $timeout(function () {
                $scope.reloadRoute();
        },500);

      }

      $scope.uploader.filters.push({
          name: 'imageFilter',
          fn: function(item /*{File|FileLikeObject}*/, options) {
              var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
              return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
          }
      });

      $scope.uploader.onAfterAddingFile = function(fileItem) {
           FotosService.addImage($routeParams.idShape, fileItem._file);
      };

      $scope.uploader.onBeforeUploadItem = function (item) {
       }
    }
  }
});
