'use strict';

angular.module('mapaFeApp').directive('abmShapesDirective', function($rootScope, LoginService, growl) {
  return {
    templateUrl: 'scripts/directives/abm-shapes-directive.html',
    restrict: 'E',
    scope: {
      idObraContratoActual: "=idObraContratoActual",
      obraContratoActual: "=obraContratoActual",
      esUnaObra: "=esUnaObra",
      idObra: "=idObra",
      estados: "="
    },
    controller: function($scope, $http, ShapesService, ObrasService, ContratosService, $routeParams, $timeout, FileUploader, growl, $route, TiposService, $location, $window, EstadosShapesService) {
      var uploader = $scope.uploader = new FileUploader({

      });
      $scope.btnGuardar = function() {
        validarShape();
      }

      $scope.btnCancelar = function() {
        $location.path('obras/');
      }

      $scope.urlFotos = '';

      if ($scope.esUnaObra) {
        $scope.urlFotosBase = "/#!/obras/" + $scope.idObra + "/shapes/";
      } else {
        //Es un contrato
        $scope.urlFotosBase = "/#!/obras/" + $scope.idObra + "/contratos/" + $scope.idObraContratoActual + "/shapes/";
      }

      $scope.tipos = 1;

      $scope.nuevoShapeBruto = {
        geometry: null,
        tipo: '',
        descripcion: ''
      };

      $scope.valoresGenericosPorCapa = {}

      TiposService.getAll().then(function(response) {
        $scope.tipos = response.data;
      });

      $scope.getTipoById = function(idTipo) {
        var tipoShape = {
          color: 'red'
        };
        angular.forEach($scope.tipos, function(tipo) {
          if (tipo.id == idTipo) {
            tipoShape = tipo;
          }
        });
        return tipoShape;
      }

      $scope.getEstadoById = function(idEstado) {
        var estadoShape = {
          codigoLinea: ''
        };
        angular.forEach($scope.estados, function(estado) {
          if (estado.id == idEstado) {
            estadoShape = estado;
          }
        });
        return estadoShape;
      }

      uploader.onAfterAddingFile = function(fileItem) {
        var arrayShapeAux = []
        window.lastItem = fileItem;
        if (fileItem.file.type === 'application/vnd.google-earth.kml+xml') {
          //llamar a funcion para convertir de kml a geojson
          var fr = new FileReader();
          fr.onload = function() {
            var xml = $.parseXML(fr.result);
            $scope.shapeGeojson = toGeoJSON.kml(xml);
            $scope.nuevoShapeBruto.geometry = $scope.shapeGeojson;
            var nuevoShape = null;
            var layers = $scope.layers;
            angular.forEach($scope.nuevoShapeBruto.geometry.features, function(shape) {
              nuevoShape = {
                descripcion: shape.properties.description || '',
                guardar: true,
                obraId: $scope.idObraContratoActual,
                geometry: shape,
                shape: null,
                tipo: '',
                layerName: shape.properties.layerName || '',
                estadoId: '',
                marcado: false,
              }

              nuevoShape.shape = L.geoJson(nuevoShape.geometry, {
                color: 'red'
              }).addTo($scope.map);
              //$scope.map.fitBounds(nuevoShape.shape.getBounds());
              var layerName = nuevoShape.layerName || 'Sin capa';
              layers[layerName] = layers[layerName] || [];
              layers[layerName].push(nuevoShape);
              arrayShapeAux.push(nuevoShape.shape);
              $scope.valoresGenericosPorCapa[layerName] = {
                descripcion: 'Descripción por defecto',
                tipo: 1,
                estado: 1,
                guardar: true,
                marcado: false,
              }

            });
            /*se genera un grupo de features con los shapes cargados
            y se hace un zoom a dicho grupo.*/
            var grupoShapes = L.featureGroup(arrayShapeAux).addTo($scope.map);
            var bounds = grupoShapes.getBounds();
            $scope.map.fitBounds(bounds);
          }
          fr.readAsText(fileItem._file);
        } else if (fileItem.file.type === 'application/zip') {
          var arrayShapeAux = []
            //llamar a la funcion que transforma de shp a geojson
          var fr = new FileReader();
          fr.onload = function() {
            $scope.shapeGeojson = shp.parseZip(fr.result);
            $scope.nuevoShapeBruto.geometry = $scope.shapeGeojson;
            var layers = $scope.layers;
            var nuevoShape = null;
            angular.forEach($scope.nuevoShapeBruto.geometry.features, function(shape) {
              nuevoShape = {
                descripcion: shape.properties.description || '',
                guardar: true,
                obraId: $scope.idObraContratoActual,
                geometry: shape,
                shape: null,
                tipo: '',
                layerName: shape.properties.layerName || '',
                estadoId: '',
                marcado: false,
              }
              nuevoShape.shape = L.geoJson(nuevoShape.geometry, {
                color: 'red'
              }).addTo($scope.map);
              //$scope.map.fitBounds(nuevoShape.shape.getBounds());
              var layerName = nuevoShape.layerName || 'Sin capa';
              layers[layerName] = layers[layerName] || [];
              layers[layerName].push(nuevoShape);
              arrayShapeAux.push(nuevoShape.shape);
              $scope.valoresGenericosPorCapa[layerName] = {
                descripcion: 'Descripción por defecto',
                tipo: 1,
                estado: 1,
                guardar: true,
                marcado: false,
              }
            });
            /*se genera un grupo de features con los shapes cargados
            y se hace un zoom a dicho grupo.*/
            var grupoShapes = L.featureGroup(arrayShapeAux).addTo($scope.map);
            var bounds = grupoShapes.getBounds();
            $scope.map.fitBounds(bounds);
          }
          fr.readAsArrayBuffer(fileItem._file);
        } else {
          growl.warning('Solo se admiten formatos .kml o los archivos shp comprimidos en un .zip');
        }
      };

      $scope.center = {
        zoom: 5,
        lat: -25.2986595,
        lng: -57.5914783
      };

      $scope.layerShape = {
        id: null,
        geometry: null
      }


      $scope.arrayShapesGuardar = [];
      $scope.$on('toggleShape', function(ev, shape) {
        if (shape.marcado) {
          var layer = shape.shape.addTo($scope.map);
          $scope.map.fitBounds(layer.getBounds());
        } else {
          $scope.map.removeLayer(shape.shape);
        }
      });

      var removeFromLayersArray = function(element) {
        for (var p in $scope.layers) {
          var co = 0;
          angular.forEach($scope.layers[p], function(shape) {
            if (shape.shape._leaflet_id == element.shape._leaflet_id) {
              $scope.layers[p].splice(co, 1);
              return;
            }
            co++;

          });
        }
      }

      $scope.$on('cambiarTipo', function(ev, shape) {
        var estado = $scope.getEstadoById(shape.estadoId);
        var tipo = $scope.getTipoById(shape.tipo);
        $scope.map.removeLayer(shape.shape);
        shape.shape = L.geoJson(shape.geometry, {
          shapeId: shape.id,
          shapeGeometry: shape.geometry,
          color: tipo.color,
          dashArray: estado.codigoLinea,
        });
        shape.shape.addTo($scope.map);
        removeFromLayersArray(shape);
        $scope.layers[tipo.descripcion].push(shape);
      });

      $scope.$on('cambiarEstado', function(ev, shape) {
        var estado = $scope.getEstadoById(shape.estadoId);
        var tipo = $scope.getTipoById(shape.tipo);
        $scope.map.removeLayer(shape.shape);
        shape.shape = L.geoJson(shape.geometry, {
          shapeId: shape.id,
          shapeGeometry: shape.geometry,
          color: tipo.color,
          dashArray: estado.codigoLinea,
          opacity: 0.8,
          fillOpacity: 0.3,
        });
        shape.shape.addTo($scope.map);

      });

      $scope.mapOptions = {
        zoomControlPosition: 'topright'
      }

      function openSidebar(pane) {
        L.control.sidebar('sidebar').open(pane);
      }

      function closeSidebar(pane) {
        L.control.sidebar('sidebar').close();
      }

      function previsualizarShape(shape) {
        var sh = L.geoJson(shape, {
          color: 'red'
        }).addTo($scope.map);
      }

      $scope.guardarShapes = function(arrayShapes) {
        $scope.$emit('guardarShapes', arrayShapes);
      }

      function validarShape() {
        //si se agrego un shape mediante el drag o selector.
        var validarCampos = true;
        for (var p in $scope.layers) {
          angular.forEach($scope.layers[p], function(shape) {
            if ((shape.guardar == true && shape.descripcion == '') || (shape.guardar == true && shape.tipo == '') || (shape.guardar == true && shape.estadoId == '')) {
              validarCampos = false;
            } else if (shape.guardar === true) {
              var newShape = {
                id: shape.id,
                geometry: shape.geometry,
                descripcion: shape.descripcion,
                tipo: {
                  id: shape.tipo
                },
                estado: {
                  id: shape.estadoId
                }
              }
              $scope.arrayShapesGuardar.push(newShape);
            }
          });
        }
        if (validarCampos) {
          //llamar a guardar shapes.
          $('#guardarShapesModal').modal('show');
          //guardarShapes($scope.arrayShapesGuardar);
        } else {
          growl.error('Debe completar todos los campos de los shapes seleccionados para poder guardarlos');
        }
      }

      function handleShapes(shapes) {
        $scope.layers = {};
        $scope.layersSinAgregar = {};
        TiposService.getAll().then(function(tiposResponse) {
          var tipos = {};
          angular.forEach(tiposResponse.data, function(tipo) {
            tipos[tipo.id] = tipo;
            $scope.layers[tipo.descripcion] = [];
            $scope.layersSinAgregar[tipo.descripcion] = [];
            $scope.valoresGenericosPorCapa[tipo.descripcion] = {
              descripcion: 'Descripción por defecto',
              tipo: 1,
              estado: 1,
              guardar: true,
              marcado: false,
            }
          });
          var estados = {};
          EstadosShapesService.getAll().then(function(estadosResponse) {
            angular.forEach(estadosResponse.data, function(estado) {
              estados[estado.id] = estado;
            });
            var shapesAZoomear = [];
            angular.forEach($scope.shapes, function(shape) {
              var tipoActual = tipos[shape.tipo];
              var estadoActual = estados[shape.estadoId];
              shape.guardar = true;
              shape.marcado = false;
              shape.shape = L.geoJson(shape.geometry, {
                //onEachFeature : onEachFeature,
                shapeId: shape.id,
                shapeGeometry: shape.geometry,
                color: tipoActual.color,
                dashArray: estadoActual.codigoLinea,
                opacity: 0.8,
                fillOpacity: 0.3,
              });

              var layerName = tipoActual.descripcion;
              $scope.layers[layerName] = $scope.layers[layerName] || [];
              $scope.layers[layerName].push(shape);
              shapesAZoomear.push(shape.shape);
              //layerSinAgregar guarda en un diccionario solo el nombre de las capas que no fueron arrastradas con el drag,es decir las que no son nuevas.
              $scope.layersSinAgregar[layerName] = layerName;
              shape.shape.addTo($scope.map);
            });
            var shapes = L.featureGroup(shapesAZoomear).addTo($scope.map);
            var bounds = shapes.getBounds();
            $scope.map.fitBounds(bounds);
          });
        });
      }

      function crearControlCapas() {
        var osm = L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="//osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo($scope.map);

        var roads = L.gridLayer.googleMutant({
          type: 'roadmap' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
        });

        var satellite = L.gridLayer.googleMutant({
          type: 'satellite' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
        });



        L.control.layers({
          "Open Street Maps": osm,
          "Google maps": roads,
          "Google earth": satellite
        }, {}, {
          position: 'topright'
        }).addTo($scope.map);
      }

      $scope.map = L.map('map', {
        zoomControl: false,
        minZoom: 7
      });
      $scope.map.setView([-25.2986595, -57.5914783], 13);
      $scope.shapes = null;
      //se cargan todos los shapes en el mapa.
      if ($scope.esUnaObra) {
        ObrasService.getAllShapes($scope.idObraContratoActual).then(function success(data) {
          $scope.shapes = data.data;
          handleShapes($scope.shapes);
        });
      } else {
        ContratosService.getAllShapes($scope.idObra, $scope.idObraContratoActual).then(function success(data) {
          $scope.shapes = data.data;
          handleShapes($scope.shapes);
        });
      }
      crearControlCapas();

    }
  }
});
