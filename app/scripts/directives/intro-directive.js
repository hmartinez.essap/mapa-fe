'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('introDirective', function() {
  return {
    restrict: 'E',
    controller: function($scope) {
      function startIntro() {
        var intro = introJs();
        intro.setOptions({
          steps: [{
            intro: "Bienvenido al Mapa Web interactivo de Obras de la ESSAP (MAWIO).<br/><br/> Este tutorial te guiará paso a paso a través de las diferentes funcionalidades disponibles. <br/>Podrás navegar fácilmente a través de la guía por medio del mouse o el teclado haciendo simplemente uso de las teclas.<br/><br/> Pulsa en siguiente para comenzar.",
          }, {
            element: document.querySelector('#menu_filtros'),
            intro: "Desplegando este menú podrá acceder a los filtros aplicables al mapa y al listado de obras.",
            position: 'right'
          }, {
            element: document.querySelector('#menu_detalle'),
            intro: "Al momento de seleccionar una obra en particular se deplegará en el panel izquierdo los detalles de la misma.",
            position: 'right'
          }, {
            element: document.querySelector('#map > div.leaflet-control-container > div.leaflet-top.leaflet-right > div.leaflet-control-zoom.leaflet-bar.leaflet-control'),
            intro: "Con este controlador podrá realizar zoom en el mapa.",
            position: 'left'
          }, {
            element: document.querySelector('.api-link'),
            intro: "A través de este botón podrá acceder a la API de desarrolladores",
            position: 'left'
          }, {
            element: document.querySelector('.ayuda'),
            intro: "A través de este botón podrá acceder a la ayuda del mapa web interactivo de obras de la ESSAP",
            position: 'left'
          },{
            element: document.querySelector('#map > div.leaflet-control-container > div.leaflet-top.leaflet-right > div.leaflet-control-layers.leaflet-control'),
            intro: "Al posicionar el cursor sobre este botón podrá seleccionar la capa base del mapa que desea utilizar.",
            position: 'left'
          }, {
            element: document.querySelector('.referencia'),
            intro: "Presionando este botón podrá visualizar las leyendas que permiten identificar a las obras por tipo y estado",
            position: 'left'
          }],
          showStepNumbers: true,
          showBullets: false,
          exitOnOverlayClick: true,
          exitOnEsc: true,
          nextLabel: 'Siguiente',
          prevLabel: 'Atrás',
          skipLabel: 'Salir',
          doneLabel: 'Gracias',
          showProgress: true,
          showButtons: true
        });
        intro.start();
      }

      $scope.$on('initIntro', function() {
        startIntro();
      })
    }
  }
});
