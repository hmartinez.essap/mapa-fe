'use strict';

angular.module('mapaFeApp').directive('comentarioDirective', function ($rootScope, ShapesService, ObrasService, ComentariosService, growl, vcRecaptchaService) {
  return {
    templateUrl : 'scripts/directives/comentario-directive.html',
    restrict : 'E',
    scope: {
      entity: '='
    },
    controller : function($scope, blockUI) {
      var widgetId;
      $scope.comentario;
      $scope.guardarComentario = function(comentario) {
        if(comentario.captcha==""){
          growl.error('Debe completar el captcha')
          return;
        }
        $scope.$broadcast('show-errors-check-validity');
        if (!$scope.form.$valid) {
          return;
        }
        $scope.comentario.shapeId = $scope.entity.shapeId;
        $scope.comentario.posicion = $scope.entity.latlng;
        blockUI.start();
        blockUI.message('Enviando...');
        ComentariosService.add($scope.comentario).then(function(response) {
          blockUI.stop();
          growl.success('Gracias por comentar, en breve recibira un correo con mas detalles.');
          $('#comentarioModal').modal('hide');
          $scope.comentario = {};
        });
      };
      $("#comentarioModal").on('hide.bs.modal', function () {
        $(this).find('form')[0].reset();
        $scope.comentario = {};
      });

      $scope.$on('mostrarComentario', function() {
        $scope.$broadcast('show-errors-reset');
        vcRecaptchaService.reload(widgetId);
        ObrasService.getObra($scope.entity.obraId).then(function(response){
            $scope.entity.nombreObra = response.data.descripcion;
        });
        $scope.comentario = {};
        $('#comentarioModal').modal('show');
      })

      $scope. onWidgetCreate = function(_widgetId){
        widgetId = _widgetId;
      };

    }
  };
});
