'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('mapaFiltro', function () {
  return {
    templateUrl : 'scripts/directives/mapa-filtro-directive.html',
    restrict : 'E',
    scope: {
      filtro: '=',
      base: '='
    },
    controller : function($scope, ShapesService,  DepartamentosService, ProveedoresService) {

      var tempFilter = {};

      var mustSetBaseFilter = false;
      function setDefaultFilter() {
        if (mustSetBaseFilter) return;
        mustSetBaseFilter = true;
        $scope.filtro.departamento = tempFilter.departamento;
        $scope.filtro.distrito = tempFilter.distrito;
        $scope.filtro.barrio = tempFilter.barrio;
        $scope.filtro.proveedor = tempFilter.proveedor;
        $scope.base = {};
      }

      function cargarDistritos(departamento) {
        DepartamentosService.getDistritos(departamento.codigo).then(function(response){
          $scope.distritos = response.data;
          if ($scope.base.distrito) {
            tempFilter.distrito = _.find($scope.distritos, { codigo : $scope.base.distrito });
            cargarBarrios(departamento, tempFilter.distrito);
            delete $scope.base.distrito;
          } else {
            setDefaultFilter();
          }
        });
      }

      function cargarBarrios(departamento, distrito) {
        DepartamentosService.getBarrios(departamento.codigo, distrito.codigo).then(function(response){
          $scope.barrios = response.data;
          $scope.barrios.sort(function(d1, d2) {
            return d1.barrioDescripcion.localeCompare(d2.barrioDescripcion);
          });
          if ($scope.base.barrio) {
            tempFilter.barrio = _.find($scope.barrios, { codigo : $scope.base.barrio });
            delete $scope.base.barrio;
          }

          setDefaultFilter();

        });
      }

      ShapesService.getAllDepartamentos().then(function(response) {
        $scope.departamentos = response.data;
        $scope.departamentos.sort(function(d1, d2) {
          return d1.nombre.localeCompare(d2.nombre);
        });
        if ($scope.base.departamento) {
          tempFilter.departamento = _.find($scope.departamentos, { codigo : $scope.base.departamento });
          cargarDistritos(tempFilter.departamento);
          delete $scope.base.departamento;
        } else {
          setDefaultFilter();
        }
      });

      ProveedoresService.getAll().then(function(response){
        $scope.proveedores = response.data;

        if ($scope.base.proveedor) {
          tempFilter.proveedor = _.find($scope.proveedores, { id : parseInt($scope.base.proveedor) }).id;
          delete $scope.base.proveedor;
        }
      });

      $scope.departamentoSelected = function() {
        cargarDistritos($scope.filtro.departamento);
        $scope.filtro.distrito = {};
        $scope.filtro.barrio = {};
        $scope.barrios = null;
      };


      $scope.distritoSelected = function() {
        $scope.filtro.barrio = '';
        if ($scope.filtro.distrito){
          cargarBarrios($scope.filtro.departamento, $scope.filtro.distrito);
        } else {
          $scope.barrios = null;
        }
      };

      $(window).on("resize.doResize", function (){
        $scope.$apply(function(){
          $scope.esChico = window.innerWidth < 768;
        });
      });

    }
  }
});
