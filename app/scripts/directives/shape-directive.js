'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('shape', function () {
  return {
    templateUrl : 'scripts/directives/shape-directive.html',
    restrict : 'E',
    scope: {
      entity: '='
    }
  }
});
