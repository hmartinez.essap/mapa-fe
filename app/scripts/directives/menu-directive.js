'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('menu', function(LoginService, $rootScope) {
  return {
    templateUrl: 'scripts/directives/menu-directive.html',
    restrict: 'E',
    replace: true,
    scope: true,
    scope: {
      isLogged: '=',
      loginHandler: '='
    },
    link: function(scope, element, attrs) {

      scope.initLogin = function() {
        scope.loginHandler.initLogin();
      }

      scope.usuarioLogeado = LoginService.getUser();

      $rootScope.$on('loginSuccess', function() {
        scope.usuarioLogeado = LoginService.getUser();
      })

      scope.logout = function() {
        scope.loginHandler.logout();
      }
    }
  };
});
