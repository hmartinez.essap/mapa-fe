'use strict';

angular.module('mapaFeApp').directive('login', function ($rootScope, LoginService, growl) {
  return {
    templateUrl : 'scripts/directives/login-directive.html',
    restrict : 'E',
    scope: {
      entity: '='
    },
    controller : function($scope) {
      $scope.data = {
        user : '',
        pass : ''
      };

      $scope.doIt = function() {
        LoginService.login($scope.data.user, $scope.data.pass).then(function success(response) {
          $scope.$emit('loginSuccess', response.data);
          $('#loginModal').modal('hide');
          growl.success('Bienvenido ' + response.data.user.nombre);
        }, function failure(response) {
          $scope.$emit('loginFail', response.data);
          growl.error('Usuario o contraseña inválidos');
        })
      };

      $scope.$on('initLogin', function() {
        $('#loginModal').modal('show');
      })

    }
  };
});
