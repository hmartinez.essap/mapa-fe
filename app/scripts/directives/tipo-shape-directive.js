'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').directive('tipoShape', function () {
  return {
    templateUrl : 'scripts/directives/tipo-shape-directive.html',
    restrict : 'E',
    scope: {
      tipos                     : '=',
      layers                    : '=currentLayers',
      nuevo                     : '=',
      nuevosfeatures            : '=',
      nuevosfeaturesconlayers   : '=',
      obra                      :'=obraActual',
      urlFotosBase              :'=urlFotosBase',
      estados                   :'=',
      layersSinAgregar          :'=layersPreCargados',
      valoresGenericosPorCapa   :'=valoresGenericosPorCapa'

    },
    controller : function($scope, $timeout) {

      $scope.toggleShape = function(shape) {
        $scope.$emit('toggleShape', shape);
      }
      $scope.checkBoxPapelera=true;

      $scope.toggleShapeGenerico = function(nombreCapa){
        angular.forEach($scope.layers[nombreCapa],function(shape){
            $scope.toggleShape(shape);
        });
      }

      $scope.cambiarTipo = function(shape) {
        $scope.$emit('cambiarTipo', shape);
      }

      $scope.cambiarEstado = function(shape) {
        $scope.$emit('cambiarEstado', shape);
      }

      $scope.getTipoById = function(idTipo){
        var tipoShape={
          color:'red'
        };
        angular.forEach($scope.tipos,function(tipo){
          if(tipo.id==idTipo){
            tipoShape = tipo;
          }
        });
        return tipoShape;
      }

      $scope.getEstadoById = function(idEstado){
        var estadoShape={
          codigoLinea:''
        };
        angular.forEach($scope.estados,function(estado){
          if(estado.id==idEstado){
            estadoShape = estado;
          }
        });
        return estadoShape;
      }

      $scope.getIcono = function(nombre) {

        var icono;
        angular.forEach($scope.tipos, function(tipo) {
          if (tipo.descripcion === nombre) icono = tipo.icono;
        })
        return icono || 'fa-question';
      }

      $scope.capaNueva= function(nombreCapa){
        //funcion que recibe un nombre de capa y verifica si es nueva,es decir
        //si es una capa recien cargada con el drag and drop o es una capa ya guardada en el sistema.
        var esCapaNueva=true;
        if($scope.layersSinAgregar[nombreCapa]){
          esCapaNueva=false;
        }
        return esCapaNueva;
      }

      $scope.restaurarSeleccionados=function(){
        for(var layerName in $scope.layers){
          angular.forEach($scope.layers[layerName],function(shape){
            if(!shape.guardar && shape.marcado){
              shape.guardar=true;
            }
          });
        }
      }

      $scope.eliminarSeleccionados=function(nombreCapa){
        angular.forEach($scope.layers[nombreCapa],function(shape){
            if(shape.marcado){
              shape.guardar=false;
            }
        });
      };

      $scope.seleccionarTodo= function(nombreCapa,valor){
        if(nombreCapa){
          angular.forEach($scope.layers[nombreCapa],function(shape){
              shape.marcado=valor;
          });
          $scope.toggleShapeGenerico(nombreCapa);
        }else{
          for(var layerName in $scope.layers){
            angular.forEach($scope.layers[layerName],function(shape){
              if(!shape.guardar){
                  shape.marcado=valor;
              }
            });
          }
        }

      }
      $scope.aplicarATodos = function(nombreCapa){
        $scope.seleccionarEstadoCapa(nombreCapa);
        $scope.cambiarDescripcionSeleccionados(nombreCapa);
        $scope.seleccionarTipoCapa(nombreCapa);
      }

      $scope.seleccionarTipoCapa= function(nombreCapa){
        //cambia de tipo a todos los shapes de una capa que esten marcados.
        var shapesFaltantes=true;
        while(shapesFaltantes){
          shapesFaltantes=false;
          angular.forEach($scope.layers[nombreCapa],function(shape){
            var shape= $scope.layers[nombreCapa][0];
            if(shape.marcado && shape.tipo!=$scope.valoresGenericosPorCapa[nombreCapa].tipo){
              shape.tipo=$scope.valoresGenericosPorCapa[nombreCapa].tipo;
              $scope.cambiarTipo(shape);
              shapesFaltantes=true;
            };
          });
        }
      /*  angular.forEach($scope.layers[nombreCapa],function(shape){
      });*/
    }

    $scope.layerIsEmpty=function(nombreCapa){
      var retorno=false;
      if($scope.layers[nombreCapa].length){
        angular.forEach($scope.layers[nombreCapa],function(shape){
          if(shape.guardar){
            retorno=true;
          }
        })
      }
      return retorno;
    }

      $scope.seleccionarEstadoCapa= function(nombreCapa){
      //cambia de estado a todos los shapes de una capa que esten marcados.
        angular.forEach($scope.layers[nombreCapa],function(shape){
          if(shape.marcado){
            shape.estadoId=$scope.valoresGenericosPorCapa[nombreCapa].estado;
            $scope.cambiarEstado(shape);
          }
        })
      }

      $scope.cambiarDescripcionSeleccionados= function(nombreCapa){
        angular.forEach($scope.layers[nombreCapa],function(shape){
          if(shape.marcado){
            shape.descripcion=$scope.valoresGenericosPorCapa[nombreCapa].descripcion;
          }
        });
      }
    }
  }
});
