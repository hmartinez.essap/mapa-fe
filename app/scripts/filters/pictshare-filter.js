'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').filter('pictshare', function() {
  return function(cadena, size) {
    return cadena.replace('PARAMS', size);
  };
})
