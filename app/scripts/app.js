'use strict';

/**
 * @ngdoc overview
 * @name mapaFeApp
 * @description
 * # mapaFeApp
 *
 * Main module of the application.
 */
angular
  .module('mapaFeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularFileUpload',
    'ui.bootstrap',
    'angular-growl',
    'ui.bootstrap.showErrors',
    'color.picker',
    'ui.select',
    'ui.utils.masks',
    'vcRecaptcha',
    'blockUI',
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/mapa', {
        templateUrl: 'views/mapa.html',
        controller: 'MapaCtrl',
        controllerAs: 'mapa',
        reloadOnSearch: false
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/usuarios', {
        templateUrl: 'views/usuarios.html',
        controller: 'UsuariosCtrl',
        require : 'ADMINISTRADOR'
      })
      .when('/usuarios/:idUser', {
        templateUrl: 'views/usuario.html',
        controller: 'UsuarioCtrl',
        require : 'ADMINISTRADOR'
      })
      .when('/tipos', {
        templateUrl: 'views/tipos.html',
        controller: 'TiposCtrl',
        require : 'CARGADOR'
      })
      .when('/tipos/:idTipo', {
        templateUrl: 'views/tipo.html',
        controller: 'TipoCtrl',
        require : 'CARGADOR'
      })
      .when('/obras', {
        templateUrl: 'views/lista_obras.html',
        controller: 'ObrasCtrl',
        controllerAs: 'obras',
        require : 'CARGADOR'
      })
      .when('/obras/:idObra', {
        templateUrl: 'views/obra.html',
        controller: 'ObraCtrl',
        require : 'CARGADOR'
      })
      .when('/obras/:idObra/shapes', {
        templateUrl: 'views/shapes-obras.html',
        controller: 'ShapesObrasCtrl',
        controllerAs: 'shapes',
        require : 'CARGADOR'
      })
      .when('/obras/:idObra/shapes/:idShape/fotos', {
        templateUrl: 'views/obras-imagenes.html',
        controller: 'obrasImagenes',
        controllerAs: 'obras-imagenes',
        require : 'CARGADOR'
      })
      .when('/obras/:idObra/contratos/', {
        templateUrl: 'views/lista_contratos.html',
        controller: 'ContratosCtrl',
        controllerAs: 'contratos',
        require : 'CARGADOR'
      })
      .when('/obras/:idObra/contratos/:idContrato', {
        templateUrl: 'views/contrato.html',
        controller: 'ContratoCtrl',
        require : 'CARGADOR'
      })
      .when('/obras/:idObra/contratos/:idContrato/shapes', {
        templateUrl: 'views/shapes-contratos.html',
        controller: 'ShapesContratosCtrl',
        require : 'CARGADOR'
      })
      .when('/obras/:idObra/contratos/:idContrato/shapes/:idShape/fotos', {
        templateUrl: 'views/contratos-imagenes.html',
        controller: 'contratosImagenes',
        require : 'CARGADOR'
      })
      .when('/contratistas', {
        templateUrl: 'views/proveedores.html',
        controller: 'ProveedoresCtrl',
        controllerAs: 'obras',
        require : 'CARGADOR'
      })
      .when('/contratistas/:idProveedor', {
        templateUrl: 'views/proveedor.html',
        controller: 'ProveedorCtrl',
        require : 'CARGADOR'
      })
      .when('/comentarios', {
        templateUrl: 'views/comentarios.html',
        controller: 'ComentariosCtrl',
        require : 'RESPONDEDOR'
      })
      .when('/estados', {
        templateUrl: 'views/estados.html',
        controller: 'EstadosCtrl',
        require : 'CARGADOR'
      })
      .when('/estados/:idEstado', {
        templateUrl: 'views/estado.html',
        controller: 'EstadoCtrl',
        require : 'CARGADOR'
      })
      .when('/comentarios/:codigo', {
        templateUrl: 'views/comentario.html',
        controller: 'ComentarioCtrl',
      })
      .when('/api-v1', {
        templateUrl: 'views/api-v1.html',
        controller: 'ApiV1Ctrl',
        controllerAs: 'apiV1'
      })
      .otherwise({
        redirectTo: '/mapa'
      });
  })
  .run(function globalStateHandler($rootScope, LoginService, growl, $location) {

    function reject() {
      growl.info("No tiene permiso para ver esta funionalidad");
      $location.path("mapa");
    }

    $rootScope.$on('$routeChangeStart', function(event, toState, toParams, fromState, fromParams, options){

        if (!toState.$$route || !toState.$$route.require) {
          return;
        }

        if (toState.$$route.require && !LoginService.getUser()) {
          reject();
        }

        if (_.isEmpty(_.filter(LoginService.getUser().roles, { rol : toState.$$route.require}))) {
          reject();
        }
    })
  })
  .constant('url', 'https://api.mawio.net/rest/')
  .config(function setInterceptors($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
  })
  .config(function(growlProvider) {

    growlProvider.globalReversedOrder(true);
    growlProvider.onlyUniqueMessages(true);
    growlProvider.globalPosition('top-center');
    growlProvider.globalDisableCountDown(true);
    growlProvider.globalTimeToLive({
      success: 5000,
      error: 2500,
      warning: 10000,
      info: 5000
    });
  })
  .config(function(blockUIConfig) {
    blockUIConfig.message = 'Cargando...';
    blockUIConfig.autoBlock = false;
  })
  .config(['showErrorsConfigProvider', function(showErrorsConfigProvider) {
    showErrorsConfigProvider.showSuccess(true);
  }])
  .constant('dtI18n', {
    sProcessing     : 'Procesando...',
    sLengthMenu     : 'Mostrar _MENU_ registros',
    sZeroRecords    : 'No se encontraron resultados',
    sEmptyTable     : 'Ningún dato disponible en esta tabla',
    sInfo           : 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
    sInfoEmpty      : 'Mostrando registros del 0 al 0 de un total de 0 registros',
    sInfoFiltered   : '(filtrado de un total de _MAX_ registros)',
    sInfoPostFix    : '',
    sSearch         : 'Buscar : ',
    sUrl            : '',
    sInfoThousands  : ',',
    sLoadingRecords : 'Cargando...',
    oPaginate: {
      sFirst:    'Primero',
      sLast:     'Último',
      sNext:     'Siguiente',
      sPrevious: 'Anterior'
    },
    oAria: {
      sSortAscending:  ': Activar para ordenar la columna de manera ascendente',
      sSortDescending: ': Activar para ordenar la columna de manera descendente'
    }
  })
  ;
