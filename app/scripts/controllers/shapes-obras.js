'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .controller('ShapesObrasCtrl', function ($scope, $routeParams, ObrasService, ShapesService, EstadosShapesService, $window, growl) {
    $scope.idObraActual = $routeParams.idObra;
    ObrasService.getObra($scope.idObraActual).then(function(data){
      $scope.obraActual =data.data;
    });

    EstadosShapesService.getAll().then(function(data){
      $scope.estados =data.data;
    });

    $scope.guardarShapes = function(){
    };

    $scope.$on('guardarShapes',function(evt,arrayShapes){
      ShapesService.updateShapesEnObras($scope.idObraActual, arrayShapes).then(function ok() {
        $window.location.reload();
      }, function err(data) {
        growl.error('Imposible guardar shape, contacte con el administrador');
        console.log(data);
      });
    });

});
