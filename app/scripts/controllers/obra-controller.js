'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ObraCtrl', function($scope, $routeParams, $location, ObrasService, growl) {
  if ($routeParams.idObra === 'new') {
    $scope.inf = 'Creando obra';
    $scope.obra = {
      visible : 'SI'
    }
  } else {
    $scope.hola = 'Editando obra';
    ObrasService.getObra($routeParams.idObra).then(function(response) {
      $scope.obra = response.data;

      $scope.obra.fechaInicio = new Date($scope.obra.fechaInicio);
      $scope.obra.fechaFin = new Date($scope.obra.fechaFin);
    });
  }

  $scope.guardar = function() {

    $scope.$broadcast('show-errors-check-validity');

    if (!$scope.form.$valid) {
      return;
    }

    ObrasService.addOrUpdate($routeParams.idObra, $scope.obra).then(function success(response) {
      $location.url('obras/');
      growl.success('Obra guardada correctamente');
    });

  }

  $scope.cancelar = function() {
    $location.path('obras');
  }
});
