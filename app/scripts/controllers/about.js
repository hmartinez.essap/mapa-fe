'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
