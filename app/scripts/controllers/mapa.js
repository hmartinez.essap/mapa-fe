'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .controller('MapaCtrl', function($scope, $location, $rootScope, ObrasService, ShapesService, FotosService, ContratosService, $timeout, DepartamentosService, TiposService, $routeParams, $compile, EstadosShapesService, growl, blockUIConfig, blockUI) {

    $scope.status = {
      open: false
    }

    function uploadSearchURL() {
      $location.search('departamento', null);
      $location.search('distrito',     null);
      $location.search('barrio',       null);
      $location.search('proveedor',    null);

      if ($scope.filtro.departamento) $location.search('departamento', $scope.filtro.departamento.codigo);
      if ($scope.filtro.distrito)     $location.search('distrito',     $scope.filtro.distrito.codigo);
      if ($scope.filtro.barrio)       $location.search('barrio',       $scope.filtro.barrio.codigo);
      if ($scope.filtro.proveedor)    $location.search('proveedor',    $scope.filtro.proveedor);
    }

    document.body.style.zoom = 1.0;
    var scale = 'scale(1)';
    document.body.style.webkitTransform = scale;
    document.body.style.msTransform = scale;
    document.body.style.transform = scale;

    $scope.cargando = true;

    $scope.shapeGlobal = {
      seleccionado: false
    }

    function showDetalles() {
      $scope.status.open = true;
      $scope.shapeGlobal.seleccionado = true;
    }

    $scope.shapeAComentar = {
      shapeId: null,
      latlng: null,
    };
    $scope.popUp = null;


    function hideDetalles() {
      if (!$scope.currentShape.id) {
        return;
      }

      $scope.currentShape = {};

      $scope.map.removeLayer($scope.marker);
      $scope.marker = null;

      $scope.status.open = false;
      $scope.shapeGlobal.seleccionado = false;

    }

    function onFeatureClick(feature, layer) {

      $scope.$apply(function() {

        var shapeId = layer.options.shapeId;
        var obraId = layer.options.obraId;

        $scope.currentShape.descripcion = {
          descripion: layer.options.descripcion
        }

        if (shapeId === $scope.currentShape.id) {
          //se hizo click para desmarcar shape
          hideDetalles();
          efectoMouseOut(layer);
          return;
        }

        $scope.shapeAComentar.latlng = feature.latlng;

        $scope.map.fitBounds(layer.getBounds());

        ShapesService.getInfo(shapeId).then(function(response) {

          var previousShape = $scope.currentShape;
          $scope.currentShape = response.data;
          $scope.currentShape.layer = layer;
          $scope.currentShape.id = shapeId;

          //se hizo click para seleccionar el shape
          if (previousShape.layer) {
            efectoMouseOut(previousShape.layer);
          }

          efectoHover(layer);
          showDetalles();

        });

      });
    }


    function onEachFeature(feature, layer) {
      layer.on({
        click: function(clickedFeature) {
          indicarShapeSeleccionado(clickedFeature.latlng, layer);
          onFeatureClick(clickedFeature, clickedFeature.layer || layer);
        },
        mouseover: function(e) {
          efectoHover(e);
        },
        mouseout: function(e) {;
          efectoMouseOut(e);
        }
      });

      layer.on('contextmenu', function(e) {

        $scope.messageContext = {
          ev: e,
          layer: layer
        };
        $scope.popUp = L.popup()
          .setLatLng(e.latlng)
          .setContent('<button class="btn btn-success" type="button" onclick="clickBotonComentario()">Realizar comentario</button>')
          .openOn($scope.map);
      });
    }

    function limpiarShapes(arrayDeShapes) {
      for (var idShape in arrayDeShapes) {
        $scope.map.removeLayer(arrayDeShapes[idShape]);
      }
    }

    function markerClick(e) {
      $scope.map.setView(e.latlng, 20);
    }

    function efectoHover(e) {

      if (e.target) { //al hacer hover se recibe un objeto con atributo target
        var geojson = $scope.diccionarioParaGuardarReferencia[e.target.options.shapeId]
      } else { //al hacer click el objeto que se recibe tiene otra estructura
        var geojson = $scope.diccionarioParaGuardarReferencia[e.options.shapeId]
      }
      marcarBorde(geojson);
    }

    $scope.pdf = function() {
      ShapesService.getAllFiltered($scope.filtro, 'list-json').then(function(data) {

        var columns = ["#", "Descripción", "Fecha Inicio", "Fecha Fin", "Tipo"];
        var rows = [];
        var obras = data.data;
        var i = 0;
        angular.forEach(obras, function(obra) {
          rows[i] = [];
          rows[i].push(i + 1);
          rows[i].push(obra.descripcion);
          rows[i].push(obra.fechaInicio);
          rows[i].push(obra.fechaFin);
          rows[i].push(obra.tipo);
          i++;
        });
        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, rows, {
          margin: {
            top: 110
          },
          addPageContent: function(data) {
            doc.text("Lista de obras", 40, 45);
            doc.setFontSize(10);
            doc.text("Fecha: " + new Date().toLocaleString(), data.settings.margin.right + 400, 20);
            doc.text("Mostrando: " + obras.length + " registros", data.settings.margin.right + 15, 800);

            if ($scope.filtro.departamento)
              doc.text("Departamento: " + $scope.filtro.departamento.nombre, 40, 60);
            if ($scope.filtro.distrito && $scope.filtro.distrito.descripcion !== undefined)
              doc.text("Distrito: " + $scope.filtro.distrito.descripcion, 40, 75);
            if ($scope.filtro.barrio && $scope.filtro.barrio.barrioDescripcion !== undefined)
              doc.text("Barrio: " + $scope.filtro.barrio.barrioDescripcion, 40, 90);
            if ($scope.filtro.proveedor && $scope.filtro.proveedor.razonSocial !== undefined)
              doc.text("Proveedor: " + $scope.filtro.proveedor.razonSocial, 40, 105);
          }
        });
        doc.save('obras.pdf');
      })

    }


    function efectoMouseOut(e) {

      var shape = e.target ? e.target.options : e.options;
      var geojson = $scope.diccionarioParaGuardarReferencia[shape.shapeId]

      if (!$scope.currentShape || $scope.currentShape.id !== shape.shapeId) {
        desmarcarBorde(geojson, shape);
      }

    }

    function marcarBorde(geojson) {
      geojson.setStyle({
        weight: 8,
        color: '#000',
        dashArray: 'continuo',
        opacity: 1,
      });
    }

    function desmarcarBorde(geojson, shape) {
      geojson.setStyle({
        onEachFeature: onEachFeature,
        color: shape.colorBorde,
        dashArray: shape.borde,
        opacity: 0.5,
        fillOpacity: 0.4,
        weight: 3
      });
    }

    function indicarShapeSeleccionado(latlng, layer) {
      var tipo = $scope.tipos[layer.options.tipo];
      if ($scope.marker) {
        $scope.map.removeLayer($scope.marker);
      }
      var icono = L.AwesomeMarkers.icon({
        icon: tipo.icono,
        markerColor: 'blue',
        prefix: 'fa'
      });
      $scope.marker = L.marker(latlng, {
        icon: icono
      }).addTo($scope.map).on('click', markerClick)
    }

    var getEstadoById = function(idEstado) {
      var estadoShape = {
        codigoLinea: ''
      };
      angular.forEach($scope.estados, function(estado) {
        if (estado.id == idEstado) {
          estadoShape = estado;
        }
      });
      return estadoShape;
    }

    window.clickBotonComentario = function() {
      $scope.shapeAComentar.shapeId = $scope.messageContext.layer.options.shapeId;
      $scope.shapeAComentar.latlng = $scope.messageContext.ev.latlng;
      $scope.shapeAComentar.obraId = $scope.messageContext.layer.options.obraId;
      $scope.$broadcast('mostrarComentario');
      $scope.popUp.remove();
    }

    window.clickBotonObraComentario = function() {
      $scope.shapeAComentar.shapeId = $scope.currentShape.id;
      $scope.shapeAComentar.obraId = $scope.currentShape.obra.id;
      $scope.$broadcast('mostrarComentario');
    }

    TiposService.getAll().then(function(data) {
      $scope.tipos = {};
      angular.forEach(data.data, function(tipo) {
        $scope.tipos[tipo.id] = tipo;
      });
    });

    $scope.legendVisible = false;


    function crearControl(posicion, clase, identificador) {

      var control = L.control({
        position: posicion
      });

      control.onAdd = function(map) {
        var div = L.DomUtil.create('div', clase);
        var template = L.DomUtil.get(identificador);
        template.parentNode.removeChild(template);
        div.appendChild(template);
        return div;
      }

      control.addTo($scope.map);
      return control;
    }

    function crearControlCapas() {
      var osm = L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="//osm.org/copyright">OpenStreetMap</a> contributors'
      }).addTo($scope.map);

      var roads = L.gridLayer.googleMutant({
        type: 'roadmap' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
      });

      var satellite = L.gridLayer.googleMutant({
        type: 'satellite' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
      });

      $(document).ready(function() {
        $scope.initIntro = function() {
          $rootScope.$broadcast('initIntro');
        }
      });

      L.control.layers({
        "Open Street Maps": osm,
        "Google maps": roads,
        "Google earth": satellite
      }, {}, {
        position: 'topright'
      }).addTo($scope.map);
    }

    function cargarControles() {

      crearControl('topright', 'loading-icon', 'loading');
      L.control.zoom({
        position: 'topright'
      }).addTo($scope.map);
      crearControl('topright', 'info legend api-link', 'api-link');
      crearControl('topright', 'info legend ayuda', 'ayuda');
      crearControl('bottomright', 'info legend referencia', 'legend');
      crearControlCapas();
    }

    $scope.diccionarioParaGuardarReferencia = {}

    function cargarEstados(cuandoHayaTodo) {
      if ($scope.estados) {
        if (cuandoHayaTodo) cuandoHayaTodo();
      } else {
        $scope.estados = {}
        EstadosShapesService.getAll().then(function(data) {
          var estados = data.data;
          angular.forEach(estados, function(estado) {
            $scope.estados[estado.id] = estado;
          });

          if (cuandoHayaTodo) cuandoHayaTodo();
        })
      }
    }

    function addShapes(diccionarioParaGuardarReferencia, datos) {

      function hacerElTrabajo() {
        angular.forEach(datos, function(shape) {
          var colorShape = $scope.tipos[shape.tipo].color;
          var estado = getEstadoById(shape.estadoId);
          var newShape = L.geoJson(shape.geometry, {
            onEachFeature: onEachFeature,
            obraId: shape.obraId,
            shapeId: shape.id,
            tipo: shape.tipo,
            stroke: true,
            color: colorShape,
            fillColor: colorShape,
            dashArray: estado.codigoLinea,
            opacity: 0.5,
            fillOpacity: 0.4,
            descripcion: shape.descripcion,
            geometry: shape.geometry,
            colorBorde: colorShape,
            borde: estado.codigoLinea,
          }).addTo($scope.map);
          diccionarioParaGuardarReferencia[shape.id] = newShape;
          $scope.diccionarioParaGuardarReferencia[shape.id] = newShape;
        });
      }
      cargarEstados(hacerElTrabajo);
    }

    var currentShapes = {};
    var shapesDeObraActual = {};

    function limpiarTodo() {
      limpiarShapes(currentShapes);
      currentShapes = {};
      $scope.obras = [];
    }

    function cargarTodo(offset, flag) {
      $scope.cargando = true;
      if (!offset) offset = 0;
      var size = 20;
      var order = "tipo_geom ASC, peso DESC";
      ShapesService.getAllFiltered($scope.filtro, 'list', offset, size, order).then(function(response) {
        if (flag !== peticionNro) {
          return;
        };
        $scope.obras = $scope.obras.concat(response.data);
        addShapes(currentShapes, response.data);
        if (response.data.length > 0)
          cargarTodo(offset + size, flag);
        else {
          var encontrado = _.find($scope.obras, { id : $scope.currentShape.id });
          if (!encontrado) {
            hideDetalles();
          }

          $scope.cargando = false;
        }
      }, function(response) {
        growl.error("Error al cargar elementos del mapa");
        $scope.cargando = false
      });
      ShapesService.getAllFiltered($scope.filtro, 'list-json').then(function(data) {
        $scope.obrasListado = data.data;
      })
    }
    $scope.csv = function csv() {
      ShapesService.getAllFiltered($scope.filtro, 'CSV');
    }
    $scope.json = function json() {
      ShapesService.getAllFiltered($scope.filtro, 'JSON');
    }

    $scope.currentShape = {
      id: null,
      layer: null,
    };

    $scope.filtro = {
      departamento: null,
      distrito: {},
      barrio: {},
      proveedor: $routeParams.proveedor || null,
    }

    if ($routeParams.departamento)
      $scope.filtro.departamento = {
        nombre: 'Cargando',
        codigo: $routeParams.departamento
      };
    else {
      $scope.filtro.departamento = {
        nombre: 'ASUNCION',
        codigo: '00'
      };
    }
    if ($routeParams.distrito)
      $scope.filtro.distrito = {
        descripcion: 'Cargando ...',
        codigo: $routeParams.distrito
      };
    if ($routeParams.barrio)
      $scope.filtro.barrio = {
        barrioDescripcion: 'Cargando ...',
        codigo: $routeParams.barrio
      };

    $scope.defaultFiltros = {
      departamento: $scope.filtro.departamento.codigo || null,
      distrito: $routeParams.distrito || null,
      barrio: $routeParams.barrio || null,
      proveedor: $routeParams.proveedor || null
    }

    uploadSearchURL();

    function long(diccionario) {
      var size = 0,
        key;
      for (key in diccionario) {
        if (diccionario.hasOwnProperty(key)) size++;
      }
      return size;
    }

    var peticionNro = 0;
    var firstFiltro = true;
    $scope.$watch('filtro', function(nuevo, viejo) {
      if (firstFiltro) {
        firstFiltro = false;
        return;
      };
      limpiarTodo();
      cargarTodo(0, ++peticionNro);
      uploadSearchURL();
    }, true);


    $timeout(function initMapa() {
      $scope.map = L.map('map', {
        zoomControl: false,
        minZoom: 6
      })

      $scope.map.setView([-25.2986595, -57.5914783], 13);

      $scope.sidebar = L.control.sidebar('sidebar', {
        position: 'left',
        closeButton: false
      });
      $scope.sidebar.addTo($scope.map);

      $timeout(function() {
        $scope.sidebar.show();
      });
      cargarControles();

    });
  });
