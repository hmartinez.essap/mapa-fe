'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('EstadoCtrl', function ($scope, EstadosService, $routeParams, growl, $location) {

  function cargarEstado(id) {
    EstadosService.getById(id).then(function(response) {
      $scope.model = response.data;
    });
  }

  function defaultData() {
    $scope.model = { };
  }

  if (!isNaN(parseInt($routeParams.idEstado))) {
    cargarEstado($routeParams.idEstado);
  } else {
    defaultData();
  }

  $scope.accept = function() {
    $scope.$broadcast('show-errors-check-validity');

    EstadosService.addOrUpdate($scope.model).then(function() {
      growl.success('Cambios guardados');
      $location.path("/estados");
    });

  };

});
