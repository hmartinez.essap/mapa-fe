'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ProveedorCtrl', function($scope, $routeParams, $location, ProveedoresService, growl) {
  if ($routeParams.idProveedor === 'new') {
    $scope.inf = 'Creando Contratista';
  } else {
    $scope.inf = 'Editando Contratista';
    ProveedoresService.getProveedor($routeParams.idProveedor).then(function(response) {
      $scope.proveedor = response.data;

    });
  }

  $scope.guardar = function() {

    $scope.$broadcast('show-errors-check-validity');

    if (!$scope.form.$valid) {
      return;
    }

    ProveedoresService.addOrUpdate($routeParams.idProveedor, $scope.proveedor).then(function success(response) {
      $location.url('contratistas/');
      growl.success('Contratista guardado correctamente');
    });

  }

  $scope.cancelar = function() {
    $location.path('contratistas');
  }
});
