'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
