'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('UsuariosCtrl', function ($scope, UsuariosService) {

  $scope.columns = [
    { title: 'Usuario'   , data: 'username'},
    { title: 'Nombre'   , data: 'nombre' },
    { title: 'Correo'   , data: 'correo'},
    { title: 'Dependencia' , data: 'dependencia'},
  ];

  $scope.service = UsuariosService;

  $scope.options = {
    addLabel       : 'usuario',
    title          : 'Lista de usuarios',
    newPath        : '#!/usuarios/new',
    editPath       : '#!/usuarios/ID',
    borrarProperty : 'nombre',
    deleteMsg      : function(data) {
      return 'Desea eliminar al usuario ' + data.nombre;
    }
  };

});
