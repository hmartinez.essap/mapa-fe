'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .controller('contratosImagenes', function ($scope, FileUploader, FotosService,$routeParams, $route, $timeout, ShapesService, ObrasService) {

     var uploader= $scope.uploader = new FileUploader({

    });
    ShapesService.getInfo($routeParams.idShape).then(function(data){
      $scope.shapeActual = data.data;
    });

    ObrasService.getObra($routeParams.idObra).then(function(data){
          $scope.obraActual =data.data;
    });

    FotosService.getFotos($routeParams.idShape).then(function success(images) {
        $scope.images=images.data;
        angular.forEach($scope.images,function(imagen){
          imagen.eliminar=false;
        });
    });

})
