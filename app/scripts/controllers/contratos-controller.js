'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ContratosCtrl', function ($scope, $routeParams, ContratosService, ObrasService) {
  $scope.columns = [
    { title: 'Nro. Llamado'   , data: 'contratoDNCP.nroContratacion',render: function(data) {
      if (!data) return '';
      return data;
    }  },
    { title: 'Cod. Contrato'   , data: 'contratoDNCP.codContratacion',render: function(data) {
      if (!data) return '';
      return data;
    }  },
    { title: 'Contratista'   , data: 'proveedor.razonSocial' },
    { title: 'Fecha Inicio'   , data: 'fechaInicio', filter_type : 'range_date',
      render: function(data) {
        return moment(data).format('DD/MM/YYYY');
      }
    },
    { title: 'Fecha fin'   , data: 'fechaFin', filter_type : 'range_date',
      render: function(data) {
        return moment(data).format('DD/MM/YYYY');
      }
    },
  ];

  $scope.service = ContratosService;

  ContratosService.getAll($routeParams.idObra).then(function(result) {
      $scope.contratos = result;
  });

  var obtenerContrato = function(id){
    var contrato = $.grep($scope.contratos.data.data, function(e){ return e.id == id; });
    $scope.contrato = contrato[0].contratoDNCP;
  }

  ObrasService.getObra($routeParams.idObra).then(function(result) {
    $scope.options.title = 'Lista de contratos de obra ' + result.data.descripcion;
  });

  $scope.options = {
    addLabel       : 'contrato',
    title          : 'Lista de contratos',
    newPath        : '#!/obras/' + $routeParams.idObra + '/contratos/new',
    editPath       : '#!/obras/' + $routeParams.idObra + '/contratos/ID',
    borrarProperty : 'id',
    backPath       : '#!/obras/',
    data           : 'contratoDNCP.codContratacion',
    getAjaxConfig  : function() {
      return ContratosService.ajaxConfig($routeParams.idObra);
    },
    otherButtons   : function(buttons, data, addLink) {
      buttons.push(addLink('Shapes', '#!/obras/' + data.obra.id + '/contratos/' + data.id+ '/shapes'));
    },
    deleteMsg : function(data) {
      obtenerContrato(data.id);
      return 'Desea eliminar el contrato ' + $scope.contrato.codContratacion + ' de ' + $scope.contrato.razonSocial + '?';
    }

  };
});
