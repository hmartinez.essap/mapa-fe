'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ComentariosCtrl', function ($scope, ComentariosService) {

  $scope.columns = [
    { title: 'Codigo'   , data: 'codigo' },
    { title: 'Estado'   , data: 'estado',
      render: function(data) {
         if (data == 'SINRESPONDER') return 'SIN RESPONDER';
           return data;
      },
      filter_config : function(base) {
        return angular.extend(base, {
          data: [ 'RESPONDIDO', 'SINRESPONDER'],
        });
      }
,    },
    { title: 'Emisor'   , data: 'email' },
    { title: 'Fecha Emisión'   , data: 'fechaEmision', filter_type : 'range_date',
      render: function(data) {
        return moment(data).format('DD/MM/YYYY');
      }
    },
    { title: 'Fecha Respuesta'   , data: 'fechaRespuesta', filter_type : 'range_date',
      render: function(data) {
        if (!data) return '';
        return moment(data).format('DD/MM/YYYY');
      }
    },
    { title: 'Usuario Respuesta' , data: 'usuarioRespuesta.nombre',
      render: function(data) {
        if (!data) return '';
        return data;
      }
    }
  ];

  $scope.service = ComentariosService;

  $scope.options = {
    addLabel       : 'Comentario',
    title          : 'Lista de comentarios',
    borrarProperty : 'codigo',
    withAgregar    : false,
    withEditar     : false,
    withEliminar   : false,

    otherButtons   : function(buttons, data, addLink) {
      if(data.estado=='SINRESPONDER'){
        buttons.push(addLink('Contestar', '#!/comentarios/' + data.codigo));
      }else{
        buttons.push(addLink('Ver', '#!/comentarios/' + data.codigo));
      }
    }
  };
});
