'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('EstadosCtrl', function ($scope, EstadosService) {

  $scope.columns = [
    { title: 'Descripcion'   , data: 'descripcion' },
    { title: 'Tipo Linea'   , data: 'tipoLinea' },
    { title: 'Codigo Linea'   , data: 'codigoLinea' },
    { title: 'Icono'   , data: 'icono'},
  ];

  $scope.service = EstadosService;

  $scope.options = {
    addLabel       : 'estado del shape',
    title          : 'Lista de estados del shapes',
    newPath        : '#!/estados/new',
    editPath       : '#!/estados/ID',
    borrarProperty : 'descripcion',
    deleteMsg      : function(data) {
      return 'Desea eliminar el estado del shape ' + data.nombre;
    }
  };

});
