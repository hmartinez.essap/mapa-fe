'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ContratoCtrl', function($scope, $routeParams, $location, ContratosService, ProveedoresService, growl) {
  var idObra = $routeParams.idObra;
  $scope.codContratacion;
  $scope.nroContratacion;
  $scope.contratista = {};

  $scope.eventoContrato = function() {
    $scope.$watch('contrato.contratoDNCP', function(nuevo, viejo) {
      if (nuevo) {
        nuevo.nroContratacion = $scope.nroContratacion;
        $scope.contrato.linkDncp = nuevo.linkDncp;
        $scope.contrato.fechaFirmaContrato = new Date(nuevo.fechaContrato);
        $scope.contratista.ruc = nuevo.ruc;
        $scope.contratista.razonSocial = nuevo.razonSocial;
        $scope.contrato.proveedor = $scope.contratista;
        $scope.contrato.montoTotal = nuevo.montoAdjudicado;
        $scope.contrato.contratoDNCP = nuevo;
      }
    }, true);
  }
  if ($routeParams.idContrato === 'new') {
    $scope.inf = 'Creando contrato';
    $scope.contrato = {
      visible: 'SI'
    }
    $scope.eventoContrato();
  } else {
    $scope.inf = 'Editando contrato';
    ContratosService.getContrato(idObra, $routeParams.idContrato).then(function(response) {
      $scope.contrato = response.data;
      $scope.nroContratacion = $scope.contrato.contratoDNCP.nroContratacion;
      $scope.codContratacion = $scope.contrato.contratoDNCP.codContratacion;
      $scope.contrato.fechaInicio = new Date($scope.contrato.fechaInicio);
      $scope.contrato.fechaFirmaContrato = new Date($scope.contrato.fechaFirmaContrato);
      $scope.contrato.fechaFin = new Date($scope.contrato.fechaFin);
    });
    $scope.eventoContrato();
  }

  $scope.obtenerContratosDNCP = function() {
    if($scope.nroContratacion == 0 || !$scope.nroContratacion){
      growl.error('Ingrese un identificador válido');
      $scope.contrato = {};
      $scope.codContratacion = null;
      $scope.nroContratacion = null;
      return;
    }
    else{
      ContratosService.getContratoDNCP($scope.nroContratacion).then(function(response) {
        $scope.contratosDNCP = response.data;
        $scope.eventoContrato();
        growl.success('Se ha obtenido el contrato correctamente');
      }, function error(data) {
        $scope.contrato = {};
        $scope.codContratacion = null;
        $scope.nroContratacion = null;
        growl.error('Ingrese un identificador valido');
      });
    }
  }
  $scope.obtenerContratosDNCPporCodigo = function(codContratacion) {
    growl.error('Ingrese el código de contratación');
    if(!$scope.nroContratacion){
      return;
    }
    ContratosService.getContratoDNCP(codContratacion).then(function(response) {
      $scope.contratoDNCP = response.data[0];
      $scope.nroContratacion = $scope.contratoDNCP.nroContratacion;
      $scope.codContratacion = $scope.contratoDNCP.codContratacion;
      $scope.contratoDNCP.nroContratacion = $scope.nroContratacion;
      $scope.contrato.linkDncp = $scope.contratoDNCP.linkDncp;
      $scope.contrato.fechaFirmaContrato = new Date($scope.contratoDNCP.fechaContrato);
      $scope.contrato.montoTotal = $scope.contratoDNCP.montoAdjudicado;
      $scope.contratista.ruc = $scope.contratoDNCP.ruc;
      $scope.contratista.razonSocial = $scope.contratoDNCP.razonSocial;
      $scope.contrato.contratoDNCP = $scope.contratoDNCP;
      $scope.contrato.proveedor = $scope.contratista;

      growl.success('Se ha obtenido el contrato correctamente');
    }, function error(mensaje) {
      $scope.contrato = {};
      $scope.codContratacion = null;
      $scope.nroContratacion = null;
      growl.error('Ingrese un identificador valido');
    });
  }

  $scope.guardar = function() {
    $scope.$broadcast('show-errors-check-validity');

    if (!$scope.form.$valid) {
      return;
    }
    if (!$scope.contrato.proveedor) {
      growl.error('Debe seleccionar un proveedor!');
      return
    }
    if ($scope.contrato.fechaInicio > $scope.contrato.fechaFin) {
      growl.error('La fecha fin no puede ser menor que la fecha inicio!');
      return
    }

    $scope.contrato.contratoDNCP.fechaContrato = new Date($scope.contrato.contratoDNCP.fechaContrato);
    $scope.contrato.obra = {
      id: $routeParams.idObra
    }

    ContratosService.addOrUpdate($routeParams.idObra, $scope.contrato).then(function success(response) {
      $location.url('obras/' + idObra + '/contratos');
      growl.success('Contrato guardado correctamente');
    });
  }

  ProveedoresService.getAll().then(function(response) {
    $scope.proveedores = response.data;
  });

  $scope.cancelar = function() {
    $location.path('obras/' + $routeParams.idObra + '/contratos');
  }
})
