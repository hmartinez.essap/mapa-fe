'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('TiposCtrl', function ($scope, TiposService) {

  $scope.columns = [
    { title: 'Descripcion'   , data: 'descripcion' },
    { title: 'Color'   , data: 'color' },
    { title: 'Icono'   , data: 'icono'},
  ];

  $scope.service = TiposService;

  $scope.options = {
    addLabel       : 'tipo de shape',
    title          : 'Lista de tipos de shapes',
    newPath        : '#!/tipos/new',
    editPath       : '#!/tipos/ID',
    borrarProperty : 'descripcion',
    deleteMsg      : function(data) {
      return 'Desea eliminar al tipo de shape ' + data.nombre;
    }
  };

});
