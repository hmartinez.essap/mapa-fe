'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ProveedoresCtrl', function ($scope, ProveedoresService) {

  $scope.columns = [
    { title: 'Razon Social'   , data: 'razonSocial' },
    { title: 'Ruc'   , data: 'ruc' },
  ];

  $scope.service = ProveedoresService;

  $scope.options = {
    addLabel       : 'contratista',
    title          : 'Lista de Contratistas',
    newPath        : '#!/contratistas/new',
    editPath       : '#!/contratistas/ID',
    borrarProperty : 'razonSocial',
    deleteMsg      : function(data) {
      return 'Desea eliminar el contratista ' + data.nombre;
    }


  };
});
