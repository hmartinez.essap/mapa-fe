
'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('UsuarioCtrl', function ($scope, UsuariosService, growl, dtI18n, $location, $routeParams) {

  function cargarUser(id) {
    UsuariosService.getById(id).then(function(response) {
      $scope.model = response.data;
      $scope.rol = $scope.rol || {};
      angular.forEach(response.data.roles, function(value) {
        $scope.rol[value.rol] = true;
      });
    });
  }

  function defaultUser() {
    $scope.model = { };
  }

  if (!isNaN(parseInt($routeParams.idUser))) {
    cargarUser($routeParams.idUser);
  } else {
    defaultUser();
  }

  $scope.$watchCollection('rol', function(old, nuevos) {
    if (old === nuevos) return;

    $scope.model.rol = [];
    angular.forEach($scope.rol, function(value, key) {
      if (value)
        $scope.model.rol.push(key);
    });
  });

  $scope.accept = function() {
    $scope.$broadcast('show-errors-check-validity');

    if (!$scope.form.$valid) {
      return;
    }

    var model = $scope.model;
    if (model.password !== model.password2) {
      growl.warning('Las contraseñas no coinciden');
      return;
    }

    if (!model.rol || model.rol.length < 1) {
      growl.warning('Debe tener al menos un rol');
      return;
    }

    UsuariosService.addOrUpdate(model).then(function() {
       growl.success('Cambios guardados');
       $location.path("/usuarios");
    });

  };
});
