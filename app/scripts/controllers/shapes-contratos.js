'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .controller('ShapesContratosCtrl', function ($scope, $routeParams, ContratosService, ShapesService, EstadosShapesService, $window, growl) {
    $scope.idContratoActual = $routeParams.idContrato;
    $scope.idObraActual= $routeParams.idObra;

    ContratosService.getContrato($routeParams.idObra,$routeParams.idContrato).then(function(data){
      $scope.contratoActual =data.data;
    });

    $scope.$on('guardarShapes',function(evt,arrayShapes){
      ShapesService.updateShapesEnContratos($scope.idObraActual,$scope.idContratoActual,arrayShapes).then(function ok() {
        $window.location.reload();
      }, function err(data) {
        growl.error('Imposible guardar shape, contacte con el administrador');
        console.log(data);
      });
    });

    EstadosShapesService.getAll().then(function(data){
      $scope.estados =data.data;
    });
  });
