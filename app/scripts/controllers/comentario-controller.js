'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ComentarioCtrl', function($scope, $routeParams, $location, ComentariosService, LoginService, growl, blockUI) {
  $scope.hola = 'Contestando Comentario';
  ComentariosService.getComentario($routeParams.codigo).then(function(response) {
    $scope.comentario = response.data;
    $scope.comentario.fechaEmision = new Date($scope.comentario.fechaEmision);
    if($scope.comentario.estado == 'SINRESPONDER'){
      $scope.comentario.estado = 'SIN RESPONDER';
    }
  });

  $scope.guardar = function() {
    if(!$scope.comentario.respuesta){
      return;
    }
    if($scope.comentario.estado == 'SIN RESPONDER'){
      $scope.comentario.estado = 'SINRESPONDER';
    }
    blockUI.start();
    blockUI.message('Enviando...');
    ComentariosService.responder($scope.comentario.codigo, $scope.comentario.respuesta).then(function success(response) {
      $location.url('comentarios/');
      growl.success('Comentario contestado');
      blockUI.stop();
    });

  }

  $scope.cancelar = function() {
    $location.path('comentarios');
  }
});
