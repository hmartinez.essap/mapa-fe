'use strict';

angular.module('mapaFeApp').controller('indexCtrl', function ($rootScope, $scope, LoginService, growl, $location) {


  if ($location.search().embedded) {
    $scope.full = false;
  } else {
    $scope.full = true;
  }

  $scope.loginHandler = {

    initLogin : function() {
      $scope.$broadcast('initLogin');
    },

    logout : function() {
      LoginService.logout().then(function() {
        growl.success('Vuelva pronto!');
        $scope.isLogged = false;
      });
    }
  };

  $rootScope.$on('loginFail', function() {
    $scope.isLogged = false;
  });

  $rootScope.$on('loginSuccess', function() {
    $scope.isLogged = true;
  });

  LoginService.checkIfLogged().then(function ok() {
    $scope.isLogged = true;
  }, function fail() {
    $scope.isLogged = false;
  });


});
