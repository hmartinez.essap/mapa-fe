'use strict';

/**
 * @ngdoc function
 * @name mawioApp.controller:ApiV1Ctrl
 * @description
 * # ApiV1Ctrl
 * Controller of the mawioApp
 */
angular.module('mapaFeApp')
  .controller('ApiV1Ctrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
