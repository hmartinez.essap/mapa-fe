'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp')
  .controller('ObrasCtrl', function ($scope, $http, ObrasService, $location, $route) {

    ObrasService.getAll().then(function success(data) {
      $scope.obras= data.data.data;
    });

    $scope.reloadRoute = function(){
      $route.reload();
    };

    $scope.eliminar = function(obraId) {
      ObrasService.delete(obraId).then(function success(response) {
        for (var i = $scope.obras.length - 1; i >= 0; i--) {
          if ($scope.obras[i].id == obraId) {
            $scope.obras.splice(i, 1);
          }
        }
      });

    };

  });
