'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('TipoCtrl', function ($scope, TiposService, $routeParams, growl, $location) {

  function cargarTipo(id) {
    TiposService.getById(id).then(function(response) {
      $scope.model = response.data;
    });
  }

  function defaultData() {
    $scope.model = { };
  }

  if (!isNaN(parseInt($routeParams.idTipo))) {
    cargarTipo($routeParams.idTipo);
  } else {
    defaultData();
  }

  $scope.accept = function() {
    $scope.$broadcast('show-errors-check-validity');

    if (!$scope.form.$valid) {
      if (!$scope.model.color) {
        growl.warning('Seleccione un color');
      }
      return;
    }

    TiposService.addOrUpdate($scope.model).then(function() {
      growl.success('Cambios guardados');
      $location.path("/tipos");
    });

  };

});
