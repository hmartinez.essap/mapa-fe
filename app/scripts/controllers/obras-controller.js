'use strict';

/**
 * @ngdoc function
 * @name mapaFeApp.controller:MapaCtrl
 * @description
 * # MapaCtrl
 * Controller of the mapaFeApp
 */
angular.module('mapaFeApp').controller('ObrasCtrl', function ($scope, ObrasService) {

  $scope.columns = [
    { title: 'Descripcion'   , data: 'descripcion' },
    { title: 'Área'   , data: 'area' },
    { title: 'Fecha Inicio'   , data: 'fechaInicio', filter_type : 'range_date',
      render: function(data) {
        return moment(data).format('DD/MM/YYYY');
      }
    },
    { title: 'Fecha fin'   , data: 'fechaFin', filter_type : 'range_date',
      render: function(data) {
        return moment(data).format('DD/MM/YYYY');
      }
    },
    { title: 'Tipo' , data: 'tipo'},
  ];

  $scope.service = ObrasService;

  $scope.options = {
    addLabel       : 'obra',
    title          : 'Lista de obras',
    newPath        : '#!/obras/new',
    editPath       : '#!/obras/ID',
    borrarProperty : 'descripcion',
    deleteMsg      : function(data) {
      return 'Desea eliminar la obra ' + data.nombre;
    },
    otherButtons   : function(buttons, data, addLink) {
      buttons.push(addLink('Contratos', '#!/obras/' + data.id + '/contratos'));
      buttons.push(addLink('Shapes', '#!/obras/' + data.id + '/shapes'));
    }

  };
});
