'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('mapaFeApp')
.factory('AuthInterceptor', function ($q) {
  return {
    request: function(config) {
      config.headers = config.headers || {};
      if (localStorage.getItem('token')) {
        config.headers.Authorization = 'Bearer ' + localStorage.getItem('token');
      }
      return config || $q.when(config);
    }
  };
});

